import {FETCH_TODO_LIST} from '../counterAction/ListTodoAction'
const intialtodos = []

export const setTodoList = (state = intialtodos, action) => {
    switch (action.type) {
        case FETCH_TODO_LIST:
            return action.payLoad
        default:
            return state
    }
}