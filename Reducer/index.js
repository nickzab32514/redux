import { combineReducers } from 'redux'
import { counterReducer } from './counterReducer'
import { todoReducer } from './todoReducer'
import { userReducer } from './userReducer'


// export const rootReducer =combineReducers({
//     counter:counterReducer
// })

// export const rootReducer =comcineReducers({
//     counterReducer:counterReducer
// })
export const rootReducer = combineReducers({
    counter: counterReducer,
    todosCompState: todoReducer,
    userCompState: userReducer

})