import React from 'react';
// import logo from './logo.svg';
import './App.css';
import CounterRedux from './components/counterRedux';
import TodoRedux from './components/TodoRedux';

// import Counter from './components/Counter';

function App() {
  return (
    <div className="App">
      {/* <Counter /> */}
      <CounterRedux />
      <TodoRedux />
    </div>
  );
}

export default App;
