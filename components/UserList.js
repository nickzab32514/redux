import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { List, Skeleton, Avatar, Input } from 'antd';
import { updateSearch } from '../Reducer/userReducer'
const { Search } = Input;


const UserList = (props) => {
    const dispatch = useDispatch();
    const { user,searchText } = props

    const setSearch = (event) => {
        // console.log(event.target.value)
        dispatch(updateSearch(event.target.value))
    }
    // console.log(user)
    console.log(searchText)

    return (
        <div>
            <Search
                placeholder="input search user"
                // onSearch={value => console.log(value)}
                style={{ width: 200 }}
                onChange={setSearch}
            />
            <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                dataSource={  
                    searchText == "" ?
                        user
                        :
                        user.filter(e => e.name.match(searchText))
                }
                renderItem={item => (
                    <List.Item
                        actions={[
                            <a href={'/users/' + item.id + '/todo'}>toDo</a>
                           ]}

                    >
                        <Skeleton avatar title={false} loading={item.loading} active>
                            <List.Item.Meta
                                avatar={
                                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                }
                                title={<a href="https://ant.design">{item.name}</a>}
                                description={<div class="row">
                                    <div>Email:{item.email}</div>
                                    <div>Phone{item.phone}</div>
                                </div>}

                            />
                        </Skeleton>

                    </List.Item>
                )}
            />
            {/* <h4>name:{listUser.name}</h4>
                        <h4>name:{listUser.username}</h4>
                        <h4>email:{listUser.email}</h4>
                        <hr /> */}
        </div>
    )
}
const mapStateToProps = state => {
    return {
        user: state.userCompState.user,
        searchText:state.userCompState.searchText
    }
}

export default connect(mapStateToProps)(UserList);
