import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Descriptions, List, Typography, Button, Select, PageHeader, Row, Col, Input } from 'antd';
// import '../ToDoPage.css';
import { fetchTodo, fetchUser,updateSearch } from '../counterAction/ListTodoAction';

const { Search } = Input;
const { Option } = Select;

const TodoRedux = () => {
    const dispatch = useDispatch();
    const dataList = useSelector(state => state.todosList)
    const user = useSelector(state => state.userList)
    const searchText = useSelector(state => state.searchList)



    useEffect(() => {
        fetchToDoData()
        fetchUserData()
    }, [])

    const fetchToDoData = () => {
        fetch('https://jsonplaceholder.typicode.com/todos?userId=1')
            .then(respose => respose.json())
            .then(data => {
                dispatch(fetchTodo(data));
            })
    }
    const fetchUserData = () => {
        fetch('https://jsonplaceholder.typicode.com/users/1')
            .then(respose => respose.json())
            .then(data => {
                dispatch(fetchUser(data));
            })
    }
    const setSearch=(name)=>{
        dispatch(searchText(name))
    }




    return (
        < div >
            <PageHeader >

                <div class="design">
                    <Row>
                        <Col >
                            <Descriptions title={"Infomation Of " + user.name} />
                        </Col>
                    </Row>
                    <Descriptions>
                        <Descriptions.Item label="UserName">{user.name}</Descriptions.Item>
                        <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                        <Descriptions.Item label="Address">{user.email}</Descriptions.Item>
                    </Descriptions>
                </div>
            </PageHeader>
            <PageHeader >

                <div class="design">
                    <Row>
                        <Descriptions title={"Infomation Of to doing list"} />
                    </Row>
                    <Row>
                        <Search
                            placeholder="input search user"
                            onChange={ (e) => 
                                setSearch(e.target.value)
                            }
                            style={{ width: 200 }}
                        />
                        <b>Type:</b>
                        <Select defaultValue="All" style={{ width: 200, paddingLeft: "10px" }} >
                            <Option value="All">All</Option>
                            <Option value="true">Done</Option>
                            <Option value="false">Doing</Option>
                        </Select>

                    </Row>
                </div>
            </PageHeader>
            <PageHeader >
                <div class="todoListStyle">
                    <List
                        header={<div class="row">
                            <th>To DoList</th>
                        </div>}
                        bordered
                        dataSource=
                        {
                            searchText==""?
                            dataList:
                            dataList.filter(e=>e.name.match(searchText))
                        }
                        renderItem={(item, index) => (
                            <List.Item>
                                <div class="col-1">
                                    {item.completed ?
                                        <Typography.Text delete>Done</Typography.Text>
                                        :
                                        <Typography.Text mark >Doing</Typography.Text>
                                    }
                                </div>
                                <div class="col-5">
                                    {item.title}
                                </div>
                                {item.completed ?
                                    <div></div>

                                    :
                                    <div class="col-5">
                                        <Button type="basic">Done</Button>
                                    </div>
                                }
                            </List.Item>
                        )}
                    />
                </div>
            </PageHeader>

        </div >
    )
}
export default TodoRedux