import React ,{useEffect}from 'react';
import { connect } from 'react-redux';
import { List, Typography, Button, Input } from 'antd';
import { updateSearchTodo,fetchTodo } from '../Reducer/todoReducer'
import { bindActionCreators } from 'redux';

const { Search } = Input;

const TodoList = (props) => {
    // const todos = useSelector(state => state.todosCompState.todos)
    // const todos = useSelector(state => state.todosCompState.data)
    const { todos, searchText ,fetchTodo,updateSearchTodo} = props
    // const userId=props.match.params.user_id
    useEffect(() => {
        fetchTodo(1)
    }, [])

    const setSearch = (event) => {
        updateSearchTodo(event.target.value)
    }
    // const setTypeTodos = (value) => {
    //     filterTodo(value)
    // }
    return (
        <div>

            <Search
                placeholder="input search user"
                // onSearch={value => console.log(value)}
                style={{ width: 200 }}
                onChange={setSearch}
            />
            <List
                header={<div class="row">
                    <th>To DoList</th>
                </div>}
                bordered
                dataSource=
                {
                    searchText == "" ?
                        todos
                        :
                        todos.filter(e => e.title.match(searchText))
                }
                renderItem={(item, index) => (
                    <List.Item>
                        <div class="col-1">
                            {item.completed ?
                                <Typography.Text delete>Done</Typography.Text>
                                :
                                <Typography.Text mark >Doing</Typography.Text>
                            }
                        </div>
                        <div class="col-5">
                            {item.title}
                        </div>
                        {item.completed ?
                                    <div></div>

                                    :
                                    <div class="col-5">
                                        <Button type="basic">Done</Button>
                                    </div>
                                }
                    </List.Item>
                )}
            />
        </div>
    )
}
const mapStateToProps = state => {
    return {
        todos: state.todosCompState.todos,
        searchText:state.todosCompState.searchText
        // filter:state.todosCompState.filter

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({fetchTodo,updateSearchTodo},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(TodoList);
