import React, { useState } from 'react';
import { useDispatch } from "react-redux"
import { addTodo } from "../counterAction/todoAction";
import { Select } from 'antd';


const { Option } = Select;

const TodoForm = () => {
    const [newTodoName, setNewTodoName] = useState('')
    const dispatch = useDispatch();
    const clickedAddHandler = () => {
        dispatch(addTodo(newTodoName));
        setNewTodoName('');
    }

    const filterTodoList=(value)=>{
        // dispatch(setNewTodoName(value));
    }

    return (
        <div>
            <h1>Todo Redux</h1>
            <Select defaultValue="ALL" style={{ width: 120 }} onChange={filterTodoList}>
                <Option value="ALL">ALL</Option>
                <Option value="DONE">DONE</Option>
                <Option value="DOING">DOING</Option>
            </Select>
            {/* <div>
                <input value={newTodoName} onChange={(e) => setNewTodoName(e.target.value)} />
                <button onClick={clickedAddHandler}>ADD</button>
            </div> */}
        </div>
    )
}
export default TodoForm