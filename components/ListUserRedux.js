import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Descriptions, List, Typography, Button, Select, PageHeader, Row, Col, Input } from 'antd';
// import '../ToDoPage.css';
import { fetchTodo } from '../counterAction/ListTodoAction';

const { Option } = Select;
const TodoRedux = () => {
    const dispatch = useDispatch();
    const dataList = useSelector(state => state.todosList)

    useEffect(() => {
        fetchToDoData()
    }, [])

    const fetchToDoData = () => {
        fetch('https://jsonplaceholder.typicode.com/user')
            .then(respose => respose.json())
            .then(data => {
                dispatch(fetchTodo(data));
            })
    }
    const fetchUserData = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(respose => respose.json())
            .then(data => {
                dispatch(fetchUser(data));
            })
    }


    return (
        < div >
            <PageHeader >

                <div class="design">
                    <Row>
                        <Descriptions title={"Infomation Of to doing list"} />
                    </Row>
                    <Row> 
                        <b>Type:</b>
                        <Select defaultValue="All" style={{ width: 200, paddingLeft: "10px" }} >
                            {/* onChange={filterTodoList} */}
                            <Option value="All">All</Option>
                            <Option value="true">Done</Option>
                            <Option value="false">Doing</Option>
                        </Select>

                    </Row>
                </div>
            </PageHeader>
            <PageHeader >
                <div class="todoListStyle">
                    <List
                        header={<div class="row">
                            <th>To DoList</th>
                        </div>}
                        bordered
                        dataSource={dataList
                        }
                        renderItem={(item, index) => (
                            <List.Item>
                                <div class="col-1">
                                    {item.completed ?
                                        <Typography.Text delete>Done</Typography.Text>
                                        :
                                        <Typography.Text mark >Doing</Typography.Text>
                                    }
                                </div>
                                <div class="col-5">
                                    {item.title}
                                </div>
                                {item.completed ?
                                    <div></div>

                                    :
                                    <div class="col-5">
                                        <Button type="basic">Done</Button>
                                    </div>
                                }
                            </List.Item>
                        )}
                    />
                </div>
            </PageHeader>

        </div >
    )
}
export default TodoRedux