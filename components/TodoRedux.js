import React, { useEffect } from 'react';

import TodoList from './TodoList';
import TodoForm from './TodoForm';
// import { fetchTodo } from '../Reducer/todoReducer';
import { useDispatch } from 'react-redux';
import { fetchUser } from '../Reducer/userReducer';
// import UserList from './UserList'

const TodoRedux = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUser())
    }, [])
    
    return(
        <div>
            <div>
                <TodoForm />
                <TodoList />
                {/* <UserList/> */}
            </div>
        </div>
    )
}
export default TodoRedux