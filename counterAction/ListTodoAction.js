export const FETCH_TODO_LIST = 'FETCH_TODO_LIST';
export const FETCH_USER = 'FETCH_USER';
export const UPDATE_SEARCH = 'UPDATE_SEARCH';


export const fetchTodo = (data) => {
    return {
        type: FETCH_TODO_LIST,
        payLoad:data
    }
}

export const fetchUser = (data) => {
    return {
        type: FETCH_USER,
        payLoad:data
    }
}

export const updateSearch = (data) => {
    return {
        type: UPDATE_SEARCH,
        payLoad:data
    }
}