import React, { useState, useEffect } from 'react';
import { Descriptions, List, Typography, Button, Select, PageHeader, Row, Col, Input } from 'antd';
import './TodoPage.css';
import { useDispatch } from 'react-redux';

const { Option } = Select;
const { Search } = Input;

const ToDoPage = () => {

    const [toDoList, setToDoList] = useState([]);
    const [selectedTypeTodo, setSelectedTypeTodo] = useState('All');

    const fetchToDoData = () => {
        fetch('https://jsonplaceholder.typicode.com/todos?userId=1' )
            .then(respose => respose.json())
            .then(data => {
                (setToDoList(data));
            })
    }

    useEffect(() => {
        fetchToDoData();
    }, []);

    const filterTodoList = (value) => {
        setSelectedTypeTodo(value);
        console.log(selectedTypeTodo)
    }

    const UpdateSearch=(event)=>{
        setToDoList(event.target.value);
        console.log(toDoList)
    }

    return (
        < div >
            <PageHeader >

                <div class="design">
                    <Row>
                            <Descriptions title={"Infomation Of to doing list"} />
                    </Row>
                    <Row>  <b>Search:</b>
                        <Search
                            placeholder="input search user"
                            onChange={UpdateSearch}
                            style={{ width: 200 }}
                        />
                        <b>Type:</b>
                        <Select defaultValue="All" style={{ width: 200, paddingLeft: "10px" }} onChange={filterTodoList}>
                            <Option value="All">All</Option>
                            <Option value="true">Done</Option>
                            <Option value="false">Doing</Option>
                        </Select>

                    </Row>
                </div>
            </PageHeader>
            <PageHeader >
                <div class="todoListStyle">
                    <List
                        header={<div class="row">
                            <th>To DoList</th>
                        </div>}
                        bordered
                        dataSource={
                            selectedTypeTodo == 'All' ?
                                toDoList
                                :
                                toDoList.filter((list) => list.completed.toString() === selectedTypeTodo)
                        }
                        renderItem={(item, index) => (
                            <List.Item>
                                <div class="col-1">
                                    {item.completed ?
                                        <Typography.Text delete>Done</Typography.Text>
                                        :
                                        <Typography.Text mark >Doing</Typography.Text>
                                    }
                                </div>
                                <div class="col-5">
                                    {item.title}
                                </div>
                                {item.completed ?
                                    <div></div>

                                    :
                                    <div class="col-5">
                                        <Button type="basic" onClick={() => {
                                            let toDoListTmp = [...toDoList]//copy object in Array

                                            toDoListTmp[item.id - 1].completed = true;

                                            setToDoList(toDoListTmp);
                                        }
                                        }>Done</Button>
                                    </div>
                                }
                            </List.Item>
                        )}
                    />
                </div>
            </PageHeader>

        </div >
    )
}
export default ToDoPage;
